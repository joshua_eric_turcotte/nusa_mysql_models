from distutils.core import setup
setup(
  name='nusa_mysql_models',
  version='0.1.0',
  author='J.E. Turcotte',
  author_email='josh@numbersusa.com',
  description="sqlalchemy 0.9 and PEP8 compliant data models for select mysql tables",
  long_description="sqlalchemy 0.9 and PEP8 compliant data models for select mysql tables",
  install_requires=["nusa",],
  package_dir={'nusa_mysql_models': 'nusa_mysql_models'},
  packages=['nusa_mysql_models',],
)
