# coding: utf-8
from sqlalchemy import BigInteger, Column, Date, DateTime, Enum, Float, Index, Integer, LargeBinary, SmallInteger, String, Table, Text
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class ActionFilter(Base):
    __tablename__ = 'action_filters'

    ID = Column(Integer, primary_key=True)
    actionID = Column(Integer, nullable=False, index=True)
    subject = Column(SmallInteger, nullable=False)
    filter = Column(Enum(u'district', u'state', u'nation'), nullable=False, server_default=u"'district'")


class Action(Base):
    __tablename__ = 'actions'

    ID = Column(SmallInteger, primary_key=True)
    type = Column(Enum(u'fax', u'meeting', u'submission', u'phonecall', u'info', u'gradecard', u'radio', u'petition'), nullable=False, index=True, server_default=u"'fax'")
    campaign = Column(Integer, nullable=False, server_default=u"'0'")
    name = Column(String(150), nullable=False, server_default=u"''")
    rationale = Column(Text, nullable=False)
    bodies = Column(Text, nullable=False)
    keywords = Column(String(255), nullable=False, server_default=u"''")
    missions = Column(Text, nullable=False)
    memo = Column(Text, nullable=False)
    sendto = Column(String(150), nullable=False, server_default=u"''")
    royname = Column(String(25), nullable=False, server_default=u"''")
    disp_order = Column(Integer, nullable=False, server_default=u"'50'")
    orglist = Column(Integer, nullable=False, server_default=u"'0'")
    list_method = Column(Enum(u'none', u'merge', u'intersect', u'exclude'), nullable=False, server_default=u"'none'")
    nextlist = Column(String(50), nullable=False)
    listfilter = Column(Integer, nullable=False, server_default=u"'0'")
    forbidden_congs = Column(Text, nullable=False)
    user_count = Column(Integer, nullable=False, server_default=u"'-1'")
    congs = Column(Text, nullable=False)
    email_id = Column(Integer, nullable=False, server_default=u"'0'")
    status = Column(Enum(u'PROCESSING', u'READY', u'COMMITTING', u'DRAFT', u'EXPIRED', u'IMPOSSIBLE', u'RESET'), nullable=False, server_default=u"'DRAFT'")
    arena = Column(SmallInteger, nullable=False, server_default=u"'0'")
    count = Column(Integer, nullable=False, server_default=u"'0'")
    hook = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    needall = Column(Integer, nullable=False, server_default=u"'0'")
    hasinterests = Column(String(100), nullable=False, server_default=u"'0'")
    notinterests = Column(String(100), nullable=False, server_default=u"'0'")
    districts = Column(Text, nullable=False)
    startat = Column(Integer, nullable=False, server_default=u"'0'")
    newuntil = Column(Integer, nullable=False, server_default=u"'0'")
    expires = Column(Integer, nullable=False, index=True, server_default=u"'0'")
    note = Column(Text, nullable=False)
    display_head = Column(String(250), nullable=False)
    display_subhead = Column(String(250), nullable=False)
    priority = Column(Integer, nullable=False, server_default=u"'3'")
    overflow_limit = Column(SmallInteger, nullable=False, server_default=u"'0'")
    extrainfo = Column(String(250), nullable=False, server_default=u"''")
    shortDescription = Column(String(250), nullable=False)
    distribute_by = Column(Enum(u'action', u'recipient type', u'recipient'), nullable=False, server_default=u"'action'")
    attachment = Column(String(250))


class ActiveFaxqueue(Base):
    __tablename__ = 'active_faxqueue'
    __table_args__ = (
        Index('dateIssued_isPE_faxDelivered_idx', 'dateIssued', 'isPE', 'faxDelivered'),
        Index('recipientID_2', 'recipientID', 'faxID', 'notBefore'),
        Index('sender_recipient_idx', 'senderID', 'recipientID'),
        Index('qs_notBefore_idx', 'queueStats', 'notBefore'),
        Index('faxDelivered', 'faxDelivered', 'recipientFax', 'queueStats', 'notBefore', 'hylafaxJobNumber'),
        Index('faxDelivered_dateDelivered_idx', 'faxDelivered', 'dateDelivered')
    )

    ID = Column(BigInteger, primary_key=True)
    isBuffet = Column(String(1))
    isPE = Column(Integer, nullable=False, server_default=u"'0'")
    body = Column(Text)
    closing = Column(Text)
    latex = Column(Text)
    ps = Column(Text)
    faxID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    recipientID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    recipientType = Column(String(1))
    recipientFax = Column(String(100))
    senderID = Column(Integer, nullable=False, server_default=u"'0'")
    dateIssued = Column(DateTime)
    dateDelivered = Column(DateTime, index=True)
    fileKey = Column(String(40))
    faxDelivered = Column(SmallInteger, server_default=u"'0'")
    errorInfo = Column(String(50))
    hylafaxJobNumber = Column(Integer, index=True, server_default=u"'0'")
    manualDelivery = Column(Integer, nullable=False, server_default=u"'0'")
    queueStats = Column(SmallInteger, nullable=False, server_default=u"'3'")
    notBefore = Column(BigInteger, index=True)
    faxServerID = Column(String(1), nullable=False, server_default=u"'0'")


class Affiliation(Base):
    __tablename__ = 'affiliations'

    ID = Column(Integer, primary_key=True)
    forType = Column(String(4), nullable=False, server_default=u"''")
    toType = Column(String(4), nullable=False, server_default=u"''")
    forID = Column(Integer, nullable=False, index=True, server_default=u"'0'")
    toID = Column(Integer, nullable=False, server_default=u"'0'")
    status = Column(SmallInteger, nullable=False, server_default=u"'0'")
    title = Column(String(60), nullable=False, server_default=u"''")
    shorttitle = Column(String(10), nullable=False, server_default=u"''")


class Campaign(Base):
    __tablename__ = 'campaigns'

    ID = Column(SmallInteger, primary_key=True)
    title = Column(String(100), nullable=False, server_default=u"''")
    description = Column(String(255), nullable=False, server_default=u"''")


class CommEmail(Base):
    __tablename__ = 'comm_email'

    ID = Column(Integer, primary_key=True)
    level_of = Column(Enum(u'primary', u'secondary', u'tertiary'), nullable=False)
    purpose = Column(String(250))
    contact = Column(String(50), nullable=False, server_default=u"'tech@numbersusa.com'")
    is_primary = Column(Integer, nullable=False, server_default=u"'0'")
    is_expensive = Column(Integer, nullable=False, server_default=u"'0'")
    r_overall = Column(Integer, nullable=False, server_default=u"'0'")
    timeMask = Column(String(1), nullable=False, server_default=u"'0'")
    entityType = Column(String(4), nullable=False, server_default=u"''")
    entityID = Column(Integer, nullable=False, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    recipID = Column(SmallInteger, nullable=False, server_default=u"'0'")


class CommFax(Base):
    __tablename__ = 'comm_faxes'

    ID = Column(Integer, primary_key=True)
    level_of = Column(Enum(u'primary', u'secondary', u'tertiary'), nullable=False)
    purpose = Column(Enum(u'capital', u'district', u'tollfree', u'unknown'), nullable=False)
    contact = Column(String(10), nullable=False, server_default=u"'0000000000'")
    is_primary = Column(Integer, nullable=False, server_default=u"'0'")
    is_expensive = Column(Integer, nullable=False, server_default=u"'0'")
    r_overnight = Column(Integer, nullable=False, server_default=u"'0'")
    r_earlymorning = Column(Integer, nullable=False, server_default=u"'0'")
    r_latemorning = Column(Integer, nullable=False, server_default=u"'0'")
    r_earlyafternoon = Column(Integer, nullable=False, server_default=u"'0'")
    r_lateafternoon = Column(Integer, nullable=False, server_default=u"'0'")
    r_earlyevening = Column(Integer, nullable=False, server_default=u"'0'")
    r_lateevening = Column(Integer, nullable=False, server_default=u"'0'")
    timeMask = Column(String(7), nullable=False, server_default=u"'0000000'")
    entityType = Column(String(4), nullable=False, server_default=u"''")
    entityID = Column(Integer, nullable=False, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    recipID = Column(SmallInteger, nullable=False, server_default=u"'0'")


class CommMail(Base):
    __tablename__ = 'comm_mail'

    ID = Column(Integer, primary_key=True)
    level_of = Column(Enum(u'primary', u'secondary', u'tertiary'), nullable=False)
    purpose = Column(String(250))
    contact = Column(String(200), nullable=False, server_default=u"'line1|line2|city|state|zip'")
    address1 = Column(String(100))
    address2 = Column(String(100))
    city = Column(String(100))
    state = Column(String(2))
    zip = Column(String(10))
    is_primary = Column(Integer, nullable=False, server_default=u"'0'")
    is_expensive = Column(Integer, nullable=False, server_default=u"'0'")
    r_overall = Column(Integer, nullable=False, server_default=u"'0'")
    timeMask = Column(String(1), nullable=False, server_default=u"'0'")
    entityType = Column(String(4), nullable=False, server_default=u"''")
    entityID = Column(Integer, nullable=False, index=True, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    recipID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")


class CommPhone(Base):
    __tablename__ = 'comm_phones'

    ID = Column(Integer, primary_key=True)
    level_of = Column(Enum(u'primary', u'secondary', u'tertiary'), nullable=False)
    purpose = Column(Enum(u'capital', u'district', u'tollfree', u'unknown'), nullable=False)
    contact = Column(String(10), nullable=False, server_default=u"'0000000000'")
    is_primary = Column(Integer, nullable=False, server_default=u"'0'")
    is_expensive = Column(Integer, nullable=False, server_default=u"'0'")
    r_overnight = Column(Integer, nullable=False, server_default=u"'0'")
    r_earlymorning = Column(Integer, nullable=False, server_default=u"'0'")
    r_latemorning = Column(Integer, nullable=False, server_default=u"'0'")
    r_earlyafternoon = Column(Integer, nullable=False, server_default=u"'0'")
    r_lateafternoon = Column(Integer, nullable=False, server_default=u"'0'")
    r_earlyevening = Column(Integer, nullable=False, server_default=u"'0'")
    r_lateevening = Column(Integer, nullable=False, server_default=u"'0'")
    timeMask = Column(String(7), nullable=False, server_default=u"'0000000'")
    entityType = Column(String(4), nullable=False, server_default=u"''")
    entityID = Column(Integer, nullable=False, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    recipID = Column(SmallInteger, nullable=False, server_default=u"'0'")


class CommTwitter(Base):
    __tablename__ = 'comm_twitter'

    ID = Column(Integer, primary_key=True)
    level_of = Column(Enum(u'primary', u'secondary', u'tertiary'), nullable=False)
    purpose = Column(String(250), nullable=False)
    contact = Column(String(50), nullable=False, server_default=u"'tech@numbersusa.com'")
    is_primary = Column(Integer, nullable=False, server_default=u"'0'")
    is_expensive = Column(Integer, nullable=False, server_default=u"'0'")
    r_overall = Column(Integer, nullable=False, server_default=u"'0'")
    timeMask = Column(String(1), nullable=False, server_default=u"'0'")
    entityType = Column(String(4), nullable=False, server_default=u"''")
    entityID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    recipID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")


class CommUrl(Base):
    __tablename__ = 'comm_urls'

    ID = Column(Integer, primary_key=True)
    level_of = Column(Enum(u'primary', u'secondary', u'tertiary'), nullable=False)
    purpose = Column(String(250), nullable=False)
    contact = Column(String(50), nullable=False, server_default=u"'tech@numbersusa.com'")
    is_primary = Column(Integer, nullable=False, server_default=u"'0'")
    is_expensive = Column(Integer, nullable=False, server_default=u"'0'")
    r_overall = Column(Integer, nullable=False, server_default=u"'0'")
    timeMask = Column(String(1), nullable=False, server_default=u"'0'")
    entityType = Column(String(4), nullable=False, server_default=u"''")
    entityID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    recipID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")


class Recipient(Base):
    __tablename__ = 'congress'

    ID = Column(SmallInteger, primary_key=True)
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, server_default=u"'0'")
    status = Column(Integer, nullable=False, server_default=u"'0'")
    party = Column(Integer, nullable=False, server_default=u"'0'")
    fname = Column(String(25), nullable=False, server_default=u"''")
    lname = Column(String(40), nullable=False, server_default=u"''")
    gender = Column(Enum(u'm', u'f'), nullable=False, server_default=u"'m'")
    religion = Column(Integer, nullable=False, server_default=u"'0'")
    marital_status = Column(Integer, nullable=False, server_default=u"'0'")
    race = Column(Integer, nullable=False, server_default=u"'0'")
    birthdate = Column(Date, nullable=False)
    title = Column(String(30), nullable=False, server_default=u"''")
    type = Column(Integer, nullable=False, server_default=u"'0'")
    faxcount = Column(Integer, nullable=False, server_default=u"'0'")
    r_responsiveness = Column(Integer, nullable=False, server_default=u"'0'")
    r_representation = Column(Integer, nullable=False, server_default=u"'0'")
    r_immigration = Column(Integer, nullable=False, server_default=u"'0'")
    aristotle_id = Column(Integer, nullable=False, server_default=u"'0'")
    govtrack_id = Column(Integer, nullable=False, server_default=u"'0'")


class District(Base):
    __tablename__ = 'district'

    ID = Column(Integer, primary_key=True, nullable=False)
    legislative_type = Column(Enum(u'district', u'state', u'state lower', u'state upper', u'nation', u'county'), nullable=False, index=True, server_default=u"'state'")
    census = Column(SmallInteger, primary_key=True, nullable=False, server_default=u"'2000'")
    abbrev = Column(String(64), nullable=False, server_default=u"''")
    name = Column(String(75), nullable=False, server_default=u"''")
    parent = Column(Integer, nullable=False, index=True, server_default=u"'0'")


class DonorHistory(Base):
    __tablename__ = 'donor_history'

    ID = Column(Integer, primary_key=True)
    from_email = Column(Integer, server_default=u"'0'")
    has_user_cookie = Column(Integer, server_default=u"'0'")
    remote_ip = Column(String(16), nullable=False, server_default=u"''")
    inserted_on = Column(DateTime, nullable=False, server_default=u'CURRENT_TIMESTAMP')
    action = Column(Enum(u'ARRIVED', u'SUBMIT', u'TRANSACTION_SUCCESS', u'TRANSACTION_FAILED'))


class FaxStyle(Base):
    __tablename__ = 'fax_styles'

    id = Column(SmallInteger, primary_key=True)
    template_category = Column(Enum(u'simple', u'header', u'footer', u'header and footer', u'custom'), nullable=False, server_default=u"'header'")
    font_size = Column(Enum(u'8pt', u'9pt', u'10pt', u'11pt', u'12pt', u'13pt', u'14pt', u'15pt', u'16pt', u'17pt', u'18pt'), nullable=False, server_default=u"'12pt'")
    paragraph_margin = Column(Enum(u'10pt', u'12pt', u'14pt', u'16pt', u'18pt', u'20pt'), nullable=False, server_default=u"'14pt'")
    line_height = Column(Enum(u'1_0', u'1_05', u'1_1', u'1_15', u'1_2', u'1_25', u'1_3pt', u'1_35', u'1_4', u'1_45', u'1_5', u'1_55', u'1_6', u'1_65', u'1_7', u'1_75', u'1_8', u'1_85', u'1_9', u'1_95', u'2'), nullable=False, server_default=u"'1_15'")
    letter_spacing = Column(Enum(u'0pt', u'0_25pt', u'0_5pt', u'0_75pt', u'1pt'), nullable=False, server_default=u"'0_25pt'")
    nth = Column(Enum(u'super', u'normal', u'none'), nullable=False, server_default=u"'normal'")
    paragraph_indent = Column(Enum(u'10pt', u'15pt', u'20pt', u'25pt', u'30pt', u'35pt'), server_default=u"'20pt'")
    font = Column(Enum(u'adobe-caslon-pro', u'arial', u'bell-mt-std', u'big-caslon', u'bookman', u'calibri', u'cambria', u'candara', u'century', u'chaparral-pro', u'constantia', u'corbel', u'courier', u'courier-new', u'franklin-gothic-book', u'frutiger', u'georgia', u'gill-sans', u'helvetica', u'helvetica-neue', u'lucida-sans', u'memphis', u'minion-pro', u'myriad-pro', u'new-baskerville', u'palatino', u'palatino-linotype', u'perpetua', u'tahoma', u'times', u'times-new-roman', u'trebuchet-ms', u'verdana'), nullable=False, server_default=u"'times-new-roman'")
    hyphenate = Column(Enum(u'false', u'true'), nullable=False, server_default=u"'false'")
    justify = Column(Enum(u'false', u'true'), nullable=False, server_default=u"'false'")
    timestamp = Column(Enum(u'header', u'body'), server_default=u"'body'")
    salut_punctuation = Column(Enum(u'', u',', u';'), nullable=False, server_default=u"','")
    valediction_lines = Column(Enum(u'', u'<BR><BR>', u'<BR><BR><BR>'), nullable=False, server_default=u"'<BR><BR><BR>'")
    header_align = Column(Enum(u'left', u'center', u'right'), nullable=False, server_default=u"'left'")
    header_display = Column(Enum(u'block', u'inline', u'hide'), nullable=False, server_default=u"'block'")
    header_border_top = Column(Enum(u'1pt', u'2pt', u'3pt', u'4pt', u'5pt', u'6pt'))
    header_border_bottom = Column(Enum(u'1pt', u'2pt', u'3pt', u'4pt', u'5pt', u'6pt'))
    header_separator = Column(Enum(u'hyphen', u'bullet', u'endash', u'emdash', u'bar', u'diamond'))
    header_name_single_line = Column(Enum(u'false', u'true'), nullable=False, server_default=u"'false'")
    header = Column(Enum(u'uppercase', u'small-caps', u'italics'))
    header_font_size = Column(Enum(u'8pt', u'9pt', u'10pt', u'11pt', u'12pt', u'13pt', u'14pt', u'15pt', u'16pt', u'17pt', u'18pt'), nullable=False, server_default=u"'14pt'")
    header_name_font_size = Column(Enum(u'8pt', u'9pt', u'10pt', u'11pt', u'12pt', u'13pt', u'14pt', u'15pt', u'16pt', u'17pt', u'18pt'))
    recipient_display = Column(Enum(u'block', u'inline'), server_default=u"'block'")
    font_header = Column(Enum(u'adobe-caslon-pro', u'arial', u'bell-mt-std', u'big-caslon', u'bookman', u'calibri', u'cambria', u'candara', u'century', u'chaparral-pro', u'constantia', u'corbel', u'courier', u'courier-new', u'franklin-gothic-book', u'frutiger', u'georgia', u'gill-sans', u'helvetica', u'helvetica-neue', u'lucida-sans', u'memphis', u'minion-pro', u'myriad-pro', u'new-baskerville', u'palatino', u'palatino-linotype', u'perpetua', u'tahoma', u'times', u'times-new-roman', u'trebuchet-ms', u'verdana'), nullable=False, server_default=u"'times-new-roman'")
    font_header_name = Column(Enum(u'adobe-caslon-pro', u'arial', u'bell-mt-std', u'big-caslon', u'bookman', u'calibri', u'cambria', u'candara', u'century', u'chaparral-pro', u'constantia', u'corbel', u'courier', u'courier-new', u'franklin-gothic-book', u'frutiger', u'georgia', u'gill-sans', u'helvetica', u'helvetica-neue', u'lucida-sans', u'memphis', u'minion-pro', u'myriad-pro', u'new-baskerville', u'palatino', u'palatino-linotype', u'perpetua', u'tahoma', u'times', u'times-new-roman', u'trebuchet-ms', u'verdana'))
    footer_align = Column(Enum(u'left', u'center', u'right'), nullable=False, server_default=u"'left'")
    footer_display = Column(Enum(u'block', u'inline', u'hide'), nullable=False, server_default=u"'hide'")
    footer_border_top = Column(Enum(u'1pt', u'2pt', u'3pt', u'4pt', u'5pt', u'6pt'))
    footer_border_bottom = Column(Enum(u'1pt', u'2pt', u'3pt', u'4pt', u'5pt', u'6pt'))
    footer_separator = Column(Enum(u'hyphen', u'bullet', u'endash', u'emdash', u'bar', u'diamond'))
    footer_name_single_line = Column(Enum(u'false', u'true'), nullable=False, server_default=u"'false'")
    footer = Column(Enum(u'uppercase', u'small-caps', u'italics'))
    footer_font_size = Column(Enum(u'8pt', u'9pt', u'10pt', u'11pt', u'12pt', u'13pt', u'14pt', u'15pt', u'16pt', u'17pt', u'18pt'), nullable=False, server_default=u"'14pt'")
    footer_name_font_size = Column(Enum(u'8pt', u'9pt', u'10pt', u'11pt', u'12pt', u'13pt', u'14pt', u'15pt', u'16pt', u'17pt', u'18pt'))
    font_footer = Column(Enum(u'adobe-caslon-pro', u'arial', u'bell-mt-std', u'big-caslon', u'bookman', u'calibri', u'cambria', u'candara', u'century', u'chaparral-pro', u'constantia', u'corbel', u'courier', u'courier-new', u'franklin-gothic-book', u'frutiger', u'georgia', u'gill-sans', u'helvetica', u'helvetica-neue', u'lucida-sans', u'memphis', u'minion-pro', u'myriad-pro', u'new-baskerville', u'palatino', u'palatino-linotype', u'perpetua', u'tahoma', u'times', u'times-new-roman', u'trebuchet-ms', u'verdana'), nullable=False, server_default=u"'times-new-roman'")
    font_footer_name = Column(Enum(u'adobe-caslon-pro', u'arial', u'bell-mt-std', u'big-caslon', u'bookman', u'calibri', u'cambria', u'candara', u'century', u'chaparral-pro', u'constantia', u'corbel', u'courier', u'courier-new', u'franklin-gothic-book', u'frutiger', u'georgia', u'gill-sans', u'helvetica', u'helvetica-neue', u'lucida-sans', u'memphis', u'minion-pro', u'myriad-pro', u'new-baskerville', u'palatino', u'palatino-linotype', u'perpetua', u'tahoma', u'times', u'times-new-roman', u'trebuchet-ms', u'verdana'))
    rule_dc_office_name = Column(Enum(u'long', u'short', u'name and abbrev', u'abbrev'), nullable=False, server_default=u"'long'")
    rule_dc_chamber_name = Column(Enum(u'long', u'medium', u'short'), nullable=False, server_default=u"'long'")
    rule_dc_repsen_title = Column(Enum(u'long-neutral', u'long-gender', u'short'), nullable=False, server_default=u"'long-neutral'")
    rule_dc_repsen_salutation = Column(Enum(u'long-neutral', u'long-gender', u'short'), nullable=False, server_default=u"'short'")
    rule_date_format = Column(Enum(u'%m/%d/%y', u'%m/%d/%Y', u'%Y-%m-%d', u'%B %d, %Y', u'%d %B %Y', u'%A, %B %d, %Y'), nullable=False, server_default=u"'%m/%d/%y'")
    rule_fax_format = Column(Enum(u'(%s) %s-%s', u'%s.%s.%s', u'%s-%s-%s'), nullable=False, server_default=u"'(%s) %s-%s'")


class Interest(Base):
    __tablename__ = 'interest'

    ID = Column(Integer, primary_key=True)
    interestname = Column(String(255), nullable=False, server_default=u"''")


class InterestCategory(Base):
    __tablename__ = 'interest_category'

    ID = Column(Integer, primary_key=True)
    category_name = Column(String(10), nullable=False, unique=True, server_default=u"'nullcat'")
    category_description = Column(String(50), nullable=False, server_default=u"'null description'")
    template = Column(String(25), nullable=False, server_default=u"'nulltemplate.html'")


class InterestHistory(Base):
    __tablename__ = 'interest_history'
    __table_args__ = (
        Index('user_id', 'user_id', 'datetime'),
    )

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    question_id = Column(Integer)
    datetime = Column(Integer)


class InterestInterestJoin(Base):
    __tablename__ = 'interest_interest_join'
    __table_args__ = (
        Index('parentchild', 'parentID', 'childID'),
    )

    ID = Column(SmallInteger, primary_key=True)
    parentID = Column(SmallInteger, nullable=False)
    childID = Column(SmallInteger, nullable=False)


class InterestTree(Base):
    __tablename__ = 'interest_tree'

    ID = Column(Integer, primary_key=True)
    interestID = Column(SmallInteger, nullable=False, server_default=u"'0'")
    unselectable = Column(Integer, nullable=False, server_default=u"'0'")
    tier = Column(Integer, nullable=False, server_default=u"'0'")
    leftID = Column(SmallInteger, nullable=False, server_default=u"'0'")
    rightID = Column(SmallInteger, nullable=False, server_default=u"'0'")


class LegislativeType(Base):
    __tablename__ = 'legislative_types'

    id = Column(Integer, primary_key=True)
    aristotle_id = Column(Integer, nullable=False, index=True, server_default=u"'-1'")
    name = Column(String(25), nullable=False)


class OrganizationLocation(Base):
    __tablename__ = 'organization_locations'

    orgID = Column(Integer, nullable=False, index=True, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, server_default=u"'0'")
    prime = Column(Integer, nullable=False, server_default=u"'0'")
    ID = Column(Integer, primary_key=True)


class OrganizationType(Base):
    __tablename__ = 'organization_types'

    ID = Column(Integer, primary_key=True, index=True)
    description = Column(Text)
    name = Column(String(100), nullable=False, server_default=u"''")
    aux1_name = Column(String(50), nullable=False, server_default=u"''")
    aux2_name = Column(String(50), nullable=False, server_default=u"''")
    aux3_name = Column(String(50), nullable=False, server_default=u"''")
    aux4_name = Column(String(50), nullable=False, server_default=u"''")
    aux5_name = Column(String(50), nullable=False, server_default=u"''")
    aux6_name = Column(String(50), nullable=False, server_default=u"''")
    aux7_name = Column(String(50), nullable=False, server_default=u"''")
    aux8_name = Column(String(50), nullable=False, server_default=u"''")
    ablock1 = Column(String(20), nullable=False, server_default=u"''")
    ablock2 = Column(String(20), nullable=False, server_default=u"''")
    ablock3 = Column(String(20), nullable=False, server_default=u"''")
    ablock4 = Column(String(20), nullable=False, server_default=u"''")


class Organization(Base):
    __tablename__ = 'organizations'

    ID = Column(Integer, primary_key=True)
    type = Column(SmallInteger, nullable=False, server_default=u"'0'")
    name = Column(String(60), nullable=False, server_default=u"''")
    aux1 = Column(Text)
    aux2 = Column(Text)
    aux3 = Column(String(250), nullable=False, server_default=u"''")
    aux4 = Column(String(100), nullable=False, server_default=u"''")
    aux5 = Column(String(50), nullable=False, server_default=u"''")
    aux6 = Column(String(20), nullable=False, server_default=u"''")
    aux7 = Column(Integer, nullable=False, server_default=u"'0'")
    aux8 = Column(Integer, nullable=False, server_default=u"'0'")
    r_responsiveness = Column(Integer, nullable=False, server_default=u"'0'")
    r_representation = Column(Integer, nullable=False, server_default=u"'0'")
    r_immigration = Column(Integer, nullable=False, server_default=u"'0'")
    stateID = Column(Integer, nullable=False, index=True, server_default=u"'0'")
    districtID = Column(Integer, nullable=False, index=True, server_default=u"'0'")
    status = Column(SmallInteger, nullable=False, server_default=u"'1'")


class OrganizationsAuxDatum(Base):
    __tablename__ = 'organizations_aux_data'

    orgID = Column(Integer, primary_key=True, nullable=False)
    orgTypeAuxID = Column(Integer, primary_key=True, nullable=False)
    value = Column(Text, nullable=False)


class Party(Base):
    __tablename__ = 'party'

    ID = Column(Integer, primary_key=True)
    abbrev = Column(String(1), nullable=False, server_default=u"''")
    party = Column(String(64), nullable=False, server_default=u"''")


class QuestionInterestJoin(Base):
    __tablename__ = 'questions_interests_join'

    id = Column(Integer, primary_key=True)
    question_id = Column(Integer)
    interest_id = Column(Integer)


class RecipientType(Base):
    __tablename__ = 'recipient_types'

    ID = Column(SmallInteger, primary_key=True)
    district_type = Column(Integer, nullable=False)
    name = Column(String(25), nullable=False)
    aristotle_ID = Column(Integer, nullable=False, server_default=u"'-1'")
    alternative_legislator_names = Column(String(200), nullable=False, server_default=u"''")
    alternative_district_names = Column(String(200), nullable=False, server_default=u"''")


class Rule(Base):
    __tablename__ = 'rules'

    ID = Column(Integer, primary_key=True)
    actionID = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    field = Column(String(50), nullable=False, index=True, server_default=u"''")
    relationShip = Column(String(10), nullable=False, server_default=u"''")
    criteria = Column(String(20), nullable=False, server_default=u"''")
    ifcondition = Column(String(10), nullable=False, server_default=u"''")
    hasvalue = Column(Integer, nullable=False, server_default=u"'0'")
    sourceTable = Column(Integer, nullable=False, server_default=u"'0'")


class SurveyQuestion(Base):
    __tablename__ = 'survey_questions'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(80))
    text = Column(String)
    status = Column(Enum(u'active', u'passive', u'disabled', u'test'), server_default=u"'disabled'")
    frequency = Column(BigInteger)


class UnacceptableContent(Base):
    __tablename__ = 'unacceptable_content'

    ID = Column(SmallInteger, primary_key=True)
    type = Column(Enum(u'review', u'kill'), nullable=False, server_default=u"'review'")
    word_regex = Column(String(80), nullable=False, server_default=u"''")
    must_match = Column(String(200), nullable=False, server_default=u"''")
    must_not_match = Column(String(200), nullable=False, server_default=u"''")


class User(Base):
    __tablename__ = 'user'
    __table_args__ = (
        Index('alive', 'alive', 'email'),
        Index('alive_last_optin_last_optout', 'alive', 'last_optin', 'last_optout'),
        Index('invalid_alive_email', 'invalidEmail', 'alive', 'email'),
        Index('alive_invalid_last_click', 'alive', 'invalidEmail', 'last_click'),
        Index('addressing', 'address', 'city', 'zip'),
        Index('username_lastname_firstname_idx', 'username', 'lastname', 'firstname'),
        Index('alive_last_firstname_email_idx', 'alive', 'lastname', 'firstname', 'email'),
        Index('firstname_phone_idx', 'firstname', 'phone'),
        Index('last_firstname_idx', 'lastname', 'firstname')
    )

    ID = Column(Integer, primary_key=True)
    uid = Column(Integer)
    username = Column(String(60), nullable=False, unique=True, server_default=u"''")
    honorific = Column(String(12), nullable=False, server_default=u"''")
    firstname = Column(String(255), nullable=False, server_default=u"''")
    lastname = Column(String(255), nullable=False, server_default=u"''")
    suffix = Column(String(10), nullable=False, server_default=u"''")
    address = Column(String(255), nullable=False, server_default=u"''")
    city = Column(String(255), nullable=False, server_default=u"''")
    state = Column(String(2), nullable=False, server_default=u"''")
    zip = Column(Integer, nullable=False, server_default=u"'00000'")
    zipfour = Column(SmallInteger, nullable=False, server_default=u"'0000'")
    phone = Column(String(16), nullable=False, server_default=u"''")
    fax = Column(String(16), nullable=False, server_default=u"''")
    email = Column(String(255), nullable=False, unique=True, server_default=u"''")
    datejoined = Column(DateTime)
    password = Column(String(255), nullable=False, server_default=u"''")
    last_visit = Column(Integer, nullable=False, server_default=u"'0'")
    last_fax = Column(Date, index=True)
    last_action = Column(DateTime, index=True)
    invalidEmail = Column(Integer, nullable=False, server_default=u"'0'")
    alive = Column(Integer, nullable=False, server_default=u"'1'")
    title = Column(String(255), nullable=False, server_default=u"''")
    fax_style = Column(Integer, nullable=False, server_default=u"'0'")
    triggers = Column(Integer, nullable=False, server_default=u"'0'")
    origin = Column(String(80), nullable=False, server_default=u"''")
    submit_count = Column(SmallInteger, nullable=False, server_default=u"'0'")
    sigstatus = Column(Enum(u'NoSig', u'Declined', u'Promised', u'InOffice', u'Scanned', u'Avail', u'Missing', u'Unreadable', u'OtherError'), nullable=False, index=True, server_default=u"'NoSig'")
    IP = Column(String(45))
    IPdate = Column(DateTime)
    linkedUserID = Column(Integer, index=True)
    fax_count = Column(SmallInteger, nullable=False, index=True, server_default=u"'0'")
    petition_count = Column(SmallInteger, nullable=False, server_default=u"'0'")
    call_count = Column(SmallInteger, nullable=False, server_default=u"'0'")
    meeting_count = Column(SmallInteger, nullable=False, server_default=u"'0'")
    click_count = Column(Integer, nullable=False, server_default=u"'0'")
    open_count = Column(Integer, nullable=False, server_default=u"'0'")
    donation_count = Column(Integer, nullable=False, server_default=u"'0'")
    donation_prior_amount = Column(Integer, nullable=False, server_default=u"'0'")
    donation_sum = Column(Integer, nullable=False, server_default=u"'0'")
    last_donation = Column(DateTime)
    last_optin = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    last_optout = Column(DateTime)
    last_bounce = Column(DateTime, index=True)
    last_open = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    last_click = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    last_action_open = Column(DateTime)
    last_action_click = Column(DateTime)
    last_news_open = Column(DateTime)
    last_news_click = Column(DateTime)
    last_webcast_open = Column(DateTime)
    last_webcast_click = Column(DateTime)
    last_meeting_open = Column(DateTime)
    last_meeting_click = Column(DateTime)
    last_fundraising_open = Column(DateTime)
    last_fundraising_click = Column(DateTime)
    last_special_open = Column(DateTime)
    last_special_click = Column(DateTime)
    last_media_open = Column(DateTime)
    last_media_click = Column(DateTime)


class UserActivity(Base):
    __tablename__ = 'user_activity'
    __table_args__ = (
        Index('aau', 'actionstat', 'actionid', 'userid'),
        Index('unique_activity', 'userid', 'actionid', 'display_order'),
        Index('actionid_actionstatus', 'actionid', 'actionstat')
    )

    id = Column(BigInteger, primary_key=True)
    userid = Column(Integer, nullable=False, server_default=u"'0'")
    actionid = Column(SmallInteger, nullable=False)
    display_order = Column(SmallInteger, nullable=False, server_default=u"'0'")
    done_recips = Column(Text, nullable=False)
    for_recips = Column(Text, nullable=False)
    actionstat = Column(Enum(u'ready', u'yes', u'maybe', u'aborted', u'failed', u'completed', u'unknownerror', u'reciperror', u'ruleerror', u'partly complete', u'no'), index=True)
    actiontime = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    taken = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    details = Column(Text, nullable=False)


class UserActivityError(Base):
    __tablename__ = 'user_activity_errors'
    __table_args__ = (
        Index('unique_activity', 'userid', 'actionid', 'display_order'),
        Index('actionid_actionstatus', 'actionid', 'actionstat')
    )

    id = Column(BigInteger, primary_key=True)
    userid = Column(Integer, nullable=False, server_default=u"'0'")
    actionid = Column(SmallInteger, nullable=False)
    display_order = Column(SmallInteger, nullable=False, server_default=u"'0'")
    done_recips = Column(Text, nullable=False)
    for_recips = Column(Text, nullable=False)
    actionstat = Column(Enum(u'ready', u'yes', u'maybe', u'aborted', u'failed', u'completed', u'unknownerror', u'reciperror', u'ruleerror', u'partly complete', u'no'), index=True)
    actiontime = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    taken = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    details = Column(String(100), nullable=False, server_default=u"''")


class UserActivityHistory(Base):
    __tablename__ = 'user_activity_history'
    __table_args__ = (
        Index('unique_activity', 'userid', 'actionid', 'display_order'),
        Index('actionid_actionstatus', 'actionid', 'actionstat')
    )

    ID = Column(BigInteger, primary_key=True)
    userid = Column(Integer, nullable=False, server_default=u"'0'")
    actionid = Column(SmallInteger, nullable=False)
    display_order = Column(SmallInteger, nullable=False, server_default=u"'0'")
    done_recips = Column(Text, nullable=False)
    for_recips = Column(Text, nullable=False)
    actionstat = Column(Enum(u'ready', u'yes', u'maybe', u'aborted', u'failed', u'completed', u'unknownerror', u'reciperror', u'ruleerror', u'partly complete', u'no'), index=True)
    actiontime = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    taken = Column(DateTime, nullable=False, server_default=u"'0000-00-00 00:00:00'")
    details = Column(String(50), nullable=False)


class UserDelegation(Base):
    __tablename__ = 'user_delegation'

    ID = Column(Integer, primary_key=True)
    state = Column(Integer, nullable=False, index=True)
    district = Column(Integer, nullable=False, index=True)
    state_upper_district = Column(Integer, nullable=False, index=True)
    state_lower_district = Column(Integer, nullable=False, index=True)
    county = Column(Integer, nullable=False, index=True)
    zip = Column(Integer, nullable=False, index=True)
    representative = Column(SmallInteger, nullable=False)
    senior_senator = Column(SmallInteger, nullable=False)
    junior_senator = Column(SmallInteger, nullable=False)
    governor = Column(SmallInteger, nullable=False)
    bishop = Column(SmallInteger, nullable=False)
    state_upper_house_1 = Column(SmallInteger, nullable=False)
    state_upper_house_2 = Column(SmallInteger, nullable=False)
    state_lower_house_1 = Column(SmallInteger, nullable=False)
    state_lower_house_2 = Column(SmallInteger, nullable=False)
    user_edited = Column(Integer, nullable=False, server_default=u"'0'")


class UserEvent(Base):
    __tablename__ = 'user_events_processing'

    id = Column(BigInteger, primary_key=True)
    user_id = Column(Integer, nullable=False, index=True)
    event = Column(Enum(u'interest change', u'address change', u'board rebuilt', u'address verified'), nullable=False, index=True, server_default=u"'address change'")
    event_time = Column(DateTime, nullable=False, server_default=u'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')


class UserInterestJoin(Base):
    __tablename__ = 'user_interest_join'

    userID = Column(Integer, primary_key=True, nullable=False, server_default=u"'0'")
    interestID = Column(Integer, primary_key=True, nullable=False, index=True, server_default=u"'0'")


class Zip(Base):
    __tablename__ = 'zip'
    __table_args__ = (
        Index('zipfour', 'zip', 'zipfour'),
    )

    id = Column(Integer, primary_key=True)
    zip = Column(Integer, nullable=False)
    zipfour = Column(SmallInteger, nullable=False)
    national_district_id = Column(Integer, nullable=False, index=True)
    state_upper_house_id = Column(Integer, nullable=False, index=True)
    state_lower_house_id = Column(Integer, nullable=False, index=True)
    county_id = Column(Integer, nullable=False)

